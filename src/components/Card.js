import React, { Component } from 'react';

import classes from './Card.module.scss';
import Day from './Day';
import Label from './Label';
import styles from './../styles/style.module.scss';
import deleteIcon from '../icons/delete-icon.svg';
import closeIcon from '../icons/close-icon.svg';

class Card extends Component {

    state = {
        activeHour: null,
        rules: [
            {hour: 6, duration: 24},
            {hour: 60, duration: 12}
        ]
    }

    handleNewInterval = (el, hour) => {
        if (document.getElementsByClassName(classes.Selector).length) {
            return;
        }
        this.setState({activeHour: hour});
        el.target.classList.add(classes.Selector);
        if (!el.target.innerHTML) el.target.innerHTML = '-';
        let selectorBox = document.getElementsByClassName(classes.SelectorBox)[0];
        selectorBox.classList.replace(classes.DisplayNone, classes.DisplayFlex);
        el.target.insertAdjacentElement('afterend',selectorBox);
    };

    removeRuleHandler = () => {
        let rules = [...this.state.rules];
        let {activeHour} = this.state;
        let index = rules.findIndex(rule => rule.hour === activeHour);
        if (index > -1) {
            try {
                document.getElementsByClassName(classes.SelectorBox)[0].classList.replace(classes.DisplayFlex, classes.DisplayNone);
            } catch (error) {
                console.log(error);
            }
            rules.splice(index, 1);
            this.setState({rules: rules, activeHour: null});
        }
    }

    cancelInterval = () => {
        try {
            document.getElementsByClassName(classes.SelectorBox)[0].classList.replace(classes.DisplayFlex, classes.DisplayNone);
        } catch (error) {
            console.log(error);
        }
        this.setState({activeHour: null});
    }

    modifyIntervalHandler = (ev) => {
        let rulesObj = [...this.state.rules];
        let {activeHour} = this.state;
        let duration = parseInt(ev.target.value);
        let index = rulesObj.findIndex(rule => rule.hour === activeHour);
        if (index >= 0) {
            rulesObj[index].duration = duration;
        } else if (duration > 0) {
            rulesObj.push({hour: activeHour, duration: duration});
        }
        rulesObj.sort((a, b) => a.hour - b.hour);
        this.setState({rules: rulesObj, activeHour: null});
        document.getElementsByClassName(classes.SelectorBox)[0].classList.replace(classes.DisplayFlex, classes.DisplayNone);
    }

    render() {
        const scheduleData = [];
        let lastObj = {};
        let disabled = 0;
        for (let i = 0; i < 168; i++) {
            if (disabled) {
                disabled--;
                scheduleData.push({type: "Disabled", hour: i});
                if (i === (lastObj.hour + lastObj.duration)) lastObj.hour = lastObj.hour + lastObj.duration;
            }
             else if (i === (lastObj.hour + lastObj.duration)) {
                scheduleData.push({type: "Interval", hour: i});
                lastObj.hour = lastObj.hour + lastObj.duration;
            } else {
                if (!this.state.rules.length) scheduleData.push({type: "Empty", hour: i});
                for (let j = 0; j < this.state.rules.length; j++) {
                    if (this.state.rules[j].hour === i) {
                        scheduleData.push({type: "Rule", duration: this.state.rules[j].duration, hour: i});
                        lastObj = {...this.state.rules[j]};
                        disabled = 6;
                        break;
                    } else if (j + 1 === this.state.rules.length) {
                        scheduleData.push({type: "Empty", hour: i});
                    } 
                }
            }
        }
        const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"].map((val, i) => {
            return <Day day={val} data={scheduleData.slice( i * 24, 24 * (1 + i) )} key={val} handleNewInterval={this.handleNewInterval} activeHour={this.state.activeHour}/>
        })
        return (
            <div className={classes.Card}>
                <h2>Set Full Spectrum Measurement Schedule</h2>
                <div className={classes.Days}>
                    {days}
                </div>
                <Label/>
                <div className={[classes.SelectorBox, classes.DisplayNone].join(" ")}>
                    <div className={classes.Triangle}></div>
                    <div className={classes.Contents}>
                        <div>
                            <h4 className={styles.LightText}>Measurement Interval</h4>
                            <img src={deleteIcon} alt="" onClick={this.removeRuleHandler}/>
                            <img src={closeIcon} alt="" onClick={this.cancelInterval}/>
                        </div>
                        <select onChange={this.modifyIntervalHandler}>
                            <option value="0">Select...</option>
                            <option value="3">3 hours</option>
                            <option value="6">6 hours</option>
                            <option value="12">12 hours</option>
                            <option value="24">24 hours</option>
                        </select>
                    </div>
                </div>
            </div>
        );
    }
}

export default Card;