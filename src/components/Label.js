import React from 'react';
import classes from './Label.module.scss';
const Label = () => {
    const nums = []; 
    for (let i = 0; i < 24; i++) {
        nums.push(i);
    }
    let mappedHours = nums.map(i => i > 9 ? <span key={i}>{i}:00</span> : <span key={i}>0{i}:00</span>); 
    
    return (
        <div className={classes.Labels}>
            <h5>Site local time (GMT -7)</h5>
            <div>
                {mappedHours}
            </div>
        </div>
    );
};

export default Label;