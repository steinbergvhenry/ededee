import React from 'react';
import classes from './Day.module.scss';
import styles from './../styles/style.module.scss';

const Day = ({day, data, handleNewInterval, activeHour}) => {

    const mappedData = data.map(el => {
        if (el.type === "Rule") {
            return <div className={[classes.CircleContainer, classes.CircleGradient].join(" ")} key={el.hour}><div className={classes.Circle} onClick={ (e) => {handleNewInterval(e, el.hour)}}>{el.duration}</div></div>;
        } else if (el.hour === activeHour) {
            return <div className={classes.CircleContainer} key={el.hour}><div className={classes.Circle}>-</div></div>;
        } else if (el.type === "Interval") {
            return <div className={classes.CircleContainer} key={el.hour}><div className={classes.Interval} onClick={ (e) => {handleNewInterval(e, el.hour)}}></div></div>;
        } else if (el.type === "Empty") {
            return <div className={classes.CircleContainer} key={el.hour}><div className={classes.EmptyCircle} onClick={ (e) => {handleNewInterval(e, el.hour)}}></div></div>;
        } else if (el.type === "Disabled") {
            return <div className={[classes.CircleContainer, classes.Disabled].join(" ")} key={el.hour}></div>;
        }
        return <h5>Err</h5>
    });

    return (
        <div className={classes.Day}>
            <div className={classes.Title}>
                <h4 className={styles.LightText}>{day}</h4>
            </div>
            <div className={classes.Data}>
                {mappedData}
            </div>
        </div>
    );
};

export default Day;